# Ws Plant Admin

Web Socket server able to control the sensors and actuators of a plant from robot-device (https://gitlab.com/uc3m-sofia/robot-device) library.



## Installation
After clone run the script 
``
./script/bootstrap
``
to resolve the submodule code dependencies.

This project system dependencies are :
  *  Boost (beast package).

Install in debian with:
``
sudo apt install libboost-all-dev
``

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Add an issue.

## Authors and acknowledgment


## License
For open source projects, say how it is licensed.

## Project status
Initial code.
