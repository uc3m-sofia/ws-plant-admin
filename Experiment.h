#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include<vector>
#include<sstream>
#include<unistd.h> //sleep

using namespace std;

class Experiment
{
public:
    Experiment(double new_Tf=0, double new_dTs =0);
    long SetParams(string new_confStream);
    long Run();


    double getTf() const;

    double getDTs() const;

private:
    double Tf;
    double dTs;
    vector<double> conf;
    stringstream confStream;

};

#endif // EXPERIMENT_H
