#include <iostream>
#include <unistd.h>

#include <iostream>
#include <utility>
#include <thread>
#include <chrono>
#include <functional>
#include <atomic>

int i=0;
// Pin Definitions
const int pin_A = 216;  // BOARD pin 7
const int pin_B = 50; // BOARD pin 11

const int pin_ain1 = 13;  // BOARD pin 27
const int pin_ain2 = 149; // BOARD pin 29
const int pin_stby = 200;   // BOARD pin 31
const int num_pwm = 2;   // BOARD pin 33

#include "GPIODevice.h"
//#include "JetsonGPIO.h"



//void (*const blinkPtr)(int);

using namespace std;

int main()
{

    double dts=0.01;

    GPIOJetson sbc;

//    QuadEncoder enc(pin_A,pin_B,sbc,28,dts);
    H2Driver drv(pin_ain1,pin_ain2,pin_stby,num_pwm);


    drv.setThrottle(20);
//    sleep(10);
    for (int i=0;i<10;i++)
    {
//        cout << "currently at pulse: " << enc.pulses << endl;
        usleep(1000*100);

    }
    drv.setThrottle(0);


//    cout << "enc stopped at pulse: " << enc.pulses << endl;
    return 0;
}
