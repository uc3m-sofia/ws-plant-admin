//
// Copyright (c) 2016-2019 Vinnie Falco (vinnie dot falco at gmail dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Official repository: https://github.com/boostorg/beast
//

//------------------------------------------------------------------------------
//
// Example: WebSocket server, synchronous
//
//------------------------------------------------------------------------------

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <string>
#include <sstream>
#include <thread>

#include "Experiment.h"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

//------------------------------------------------------------------------------

#include "Plant.h"
#include "GPIODevice.h"

#include "fcontrol.h"

double dts=0.01;
double sendValue=150.0;
double *ws_send = &sendValue;
std::string ws_send_str;
// Pin Definitions
const int pin_A = 74;  // BOARD pin 5
const int pin_B = 216; // BOARD pin 7

const int pin_ain1 = 13;  // BOARD pin 27
const int pin_ain2 = 149; // BOARD pin 29
const int pin_stby = 200;   // BOARD pin 31
const int num_pwm = 2;   // BOARD pin 33
GPIOJetson board;
QuadEncoder enc(pin_A, pin_B, board, 28, dts);
H2Driver drv(pin_ain1, pin_ain2, pin_stby, num_pwm);
SamplingTime Ts(dts);

//Plant p1(&drv, &enc,dts);
Experiment exp1;
string newCommand;

double paramValue;
long result;

PIDBlock a(1,1,1,0.1);

// Socket main session
void do_session(tcp::socket socket)
{
    try
    {
        // Construct the stream by moving in the socket
        websocket::stream<tcp::socket> ws{std::move(socket)};

        // Set a decorator to change the Server of the handshake
        ws.set_option(websocket::stream_base::decorator(
            [](websocket::response_type& res)
            {
                res.set(http::field::server,
                    std::string(BOOST_BEAST_VERSION_STRING) +
                        " websocket-server-sync");
            }));

        // Accept the websocket handshake
        ws.accept();
        beast::flat_buffer buffer;
        stringstream command;
        string paramName;
        for(;;)
        {

//            buffer.clear(); //Not working?
//            buffer.str("");
            // Read a message
            ws.read(buffer);

//            std::cout << beast::make_printable(buffer.data()) << std::endl;

            //reset the stringstream variable
            command.str("");
            //put buffer data into a stringstream
            command << beast::make_printable(buffer.data());
//            newCommand = command.str();

            //remove data from buffer
            buffer.consume(buffer.size());

            command >> paramName;
//            std::cout << paramName << std::endl;

            if(paramName == "config")
            {
//                paramName = command.str();
                result = exp1.SetParams(command.str());
//                std::cout << command.str() << std::endl;
                std::cout << "result in config: " << result  << std::endl;
            }
            else if(paramName == "runSim")
            {
                ws.write(net::buffer("runSimStart"));
                exp1.Run();
                ws.write(net::buffer("runSimStop"));

            }
            //unknown command
            else
            {
                 std::cout << command.str() << std::endl;
            }

//            for(int i =0 ; i<50;i++)
//            {
//                sendValue=enc.pulses;
//                ws_send_str=to_string(*ws_send);
//                // This buffer will hold the message
//                ws.write(net::buffer(ws_send_str));
//                usleep(0.01*1000*1000);
//            }


        }

    }

    catch(beast::system_error const& se)
    {
        // This indicates that the session was closed
        if(se.code() != websocket::error::closed)
            std::cerr << "Error: " << se.code().message() << std::endl;
    }
    catch(std::exception const& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}


// Socket main session
void plant_session(tcp::socket socket)
{
    try
    {
        // Construct the stream by moving in the socket
        websocket::stream<tcp::socket> ws{std::move(socket)};

        // Set a decorator to change the Server of the handshake
        ws.set_option(websocket::stream_base::decorator(
            [](websocket::response_type& res)
            {
                res.set(http::field::server,
                    std::string(BOOST_BEAST_VERSION_STRING) +
                        " websocket-server-sync");
            }));

        // Accept the websocket handshake
        ws.accept();
        beast::flat_buffer buffer;
        stringstream command;
        string paramName;


        for(;;)
        {

            std::cout << "Plant ready" << std::endl;
            // Read a message
            ws.read(buffer);
            std::cout << beast::make_printable(buffer.data()) << std::endl;

            //reset the stringstream variable
            command.str("");
            //put buffer data into a stringstream
            command << beast::make_printable(buffer.data());
//            newCommand = command.str();

            //remove data from buffer
            buffer.consume(buffer.size());

            paramName = command.str();
            command >> paramName;
//            std::cout << paramName << std::endl;

            if(paramName == "run")
            {
                drv.setThrottle(20);

                Ts.SetSamplingTime(exp1.getDTs());
                for(double t=0; t<exp1.getTf(); t+=exp1.getDTs())
                {
                    ws.write(net::buffer(to_string(enc.get_vel())));
                    Ts.WaitSamplingTime();
                    //                usleep(1000*1000*exp1.getDTs());

                }
            }
            drv.setThrottle(00);

        }

    }

    catch(beast::system_error const& se)
    {
        // This indicates that the session was closed
        if(se.code() != websocket::error::closed)
            std::cerr << "Error: " << se.code().message() << std::endl;
    }
    catch(std::exception const& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
    }
}

//------------------------------------------------------------------------------
double run(PIDBlock &controller ,Plant &planta,double &target,double &error, double &controlSignal){
//    error=target-planta.out();
    controlSignal=controller.OutputUpdate(error);
//    planta.in(&controlSignal);
    return planta.x;
}

int main(int argc, char* argv[])
{

    double controlSignal=0;
//    p1.in(&controlSignal);
//    sleep(3);
//    controlSignal=0;
//    p1.in(&controlSignal);
//    double pos=p1.out();
//    std::cout<<"posicion final"<<pos<<std::endl;
//    a.AntiWindup(1,1);
//    double error=10000;
//    double target=1000;
//    for(;error>1;){
//        run(a,p1,target,error,controlSignal);
//    }

    drv.enable();
    try
    {
        // Check command line arguments.
        if (argc != 3)
        {
            std::cerr <<
                "Usage: websocket-server-sync <address> <port>\n" <<
                "Example:\n" <<
                "    websocket-server-sync 0.0.0.0 8080\n";
            return EXIT_FAILURE;
        }
        auto const address = net::ip::make_address(argv[1]);
        auto const port = static_cast<unsigned short>(std::atoi(argv[2]));
        auto const portPlant = static_cast<unsigned short>(8081);

        // The io_context is required for all I/O
        net::io_context ioc{1};
        net::io_context iocp{1};

        // The acceptor receives incoming connections
        tcp::acceptor acceptor{ioc, {address, port}};
        tcp::acceptor acceptorPlant{iocp, {address, portPlant}};



        for(;;)
        {
            // This will receive the new connection
            tcp::socket socket{ioc};

            // Block until we get a connection
            acceptor.accept(socket);

            // Launch the session, transferring ownership of the socket
            std::thread(
                        &do_session,
                        std::move(socket)).detach();

            sleep(1);
            // This will receive the new connection
            tcp::socket socketPlant{iocp};

            // Block until we get a connection
            acceptorPlant.accept(socketPlant);

            // Launch the session, transferring ownership of the socket
            std::thread(
                        &plant_session,
                        std::move(socketPlant)).detach();
        }




//        drv.enable();
//        drv.setThrottle(20);

//        for(;;)
//        {
//           *ws_send=enc.get_pos()*0.22;
//        }


    }
    catch (const std::exception& e)
    {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }
}
