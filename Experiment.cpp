#include "Experiment.h"

Experiment::Experiment(double new_Tf, double new_dTs)
{
    conf.resize(2);

}

long Experiment::SetParams(string new_confStream)
{
    vector<double> newConf(0);
    double newValue;
    confStream = stringstream(new_confStream);

    confStream.ignore(7,' ');

    while (confStream >> newValue)
    {
        newConf.push_back(newValue);
    }
    if(newConf.size()!=conf.size())
    {
        return -1;
    }
    conf = newConf;

    Tf=conf[0];
    dTs=conf[1];
    return 0;

}

long Experiment::Run()
{
//    sleep(Tf);
    return 0;

}

double Experiment::getTf() const
{
    return Tf;
}

double Experiment::getDTs() const
{
    return dTs;
}


